package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	 response.setContentType("text/html");
	 PrintWriter out = response.getWriter();
	 
	 String studentId = request.getParameter("studentId");
	 String studentName = request.getParameter("studentName");
	 String course = request.getParameter("course");
	 double fees = Double.parseDouble(request.getParameter("fees"));
	 String gender = request.getParameter("gender");
	 String emailId = request.getParameter("emailId");
	String password = request.getParameter("password");
		System.out.println("StudentId : " + studentId);
		System.out.println("StudentName : " + studentName);
		System.out.println("Course : " + course);
		System.out.println("Fees : " + fees);
		System.out.println("Gender : " + gender);
		System.out.println("EmailId : " + emailId);
		System.out.println("Password: " + password);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
