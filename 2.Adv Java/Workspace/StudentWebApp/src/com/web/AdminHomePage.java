package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/AdminHomePage")
public class AdminHomePage extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	     response.setContentType("text/html");
	     PrintWriter out = response.getWriter();
	     String emailId = request.getParameter("emailId");
	     out.print("<html><body bgcolor = 'green' text = 'pink'>");
	     out.print("<h3>Welcome " + emailId + "! </h3>");
	     out.print("<form align='right'>");
		 out.print("<a href='Login.html'>Logout</a>");
		 out.print("</form>");
	     out.print("<centre>");
	     out.print("<h1>Welcome To AdminHomePage</h1>");
	     out.print("</centre></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
