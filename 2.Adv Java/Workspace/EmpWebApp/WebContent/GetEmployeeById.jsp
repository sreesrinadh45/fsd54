<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetEmployeeById</title>
</head>
<body>

<%@ include file="HRHomePage.jsp" %>

<br/>
<center>
<form action="GetEmployeeById">

	<table>
		<tbody>
			<tr>
				<td>Enter Employee Id:</td>
				<td><input type="text" name="empId"  /></td>
			</tr>			
			<tr>
				<td colspan="2" align="center">
					<button>Get Employee</button>
				</td>
			</tr>
		</tbody>
	</table>
</form>
</center>
</body>
</html>