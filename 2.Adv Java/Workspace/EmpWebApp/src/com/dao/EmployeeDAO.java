package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Employee;

public class EmployeeDAO {

	public Employee empLogin(String emailId, String password) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		if (con == null) {
			System.out.println("Unable to Establish the Connection!!!");
			return null;
		}

		try {
			String qry = "Select * from employee where emailId=? and password=?";
			pst = con.prepareStatement(qry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();

			if (rs.next()) {

				Employee emp = new Employee();

				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));

				return emp;

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public int empRegister(Employee emp) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;

		if (con == null) {
			System.out.println("Unable to Establish the Connection!!!");
			return 0;
		}

		try {
			String qry = "insert into employee (empName, salary, gender, emailId, password) values (?, ?, ?, ?, ?)";

			pst = con.prepareStatement(qry);
			pst.setString(1, emp.getEmpName());
			pst.setDouble(2, emp.getSalary());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());

			int result = pst.executeUpdate();
			return result;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;

	}

	public List<Employee> getAllEmployees() {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		if (con == null) {
			System.out.println("Unable to Establish the Connection!!!");
			return null;
		}

		try {
			String qry = "Select * from employee";
			pst = con.prepareStatement(qry);
			;
			rs = pst.executeQuery();

			List<Employee> empList = new ArrayList<Employee>();

			while (rs.next()) {

				Employee emp = new Employee();

				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));

				empList.add(emp);
			}

			return empList;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Employee getEmployeeById(int empId) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		if (con == null) {
			System.out.println("Unable to Establish the Connection!!!");
			return null;
		}

		try {
			String qry = "Select * from employee where empId=?";
			pst = con.prepareStatement(qry);
			pst.setInt(1, empId);
			rs = pst.executeQuery();

			if (rs.next()) {

				Employee emp = new Employee();

				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));

				return emp;

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public int updateEmployee(Employee emp) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;

		if (con == null) {
			System.out.println("Unable to Establish the Connection!!!");
			return 0;
		}

		try {
			String qry = "update employee set empName=?, salary=?, gender=?, emailId=?, password=? where empId=?";

			pst = con.prepareStatement(qry);
			pst.setString(1, emp.getEmpName());
			pst.setDouble(2, emp.getSalary());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
			pst.setInt(6, emp.getEmpId());

			return pst.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}

public int deleteEmployee(int empId) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String qry = "delete from employee where empId = ?";
		int result = 0;
				
		if (con == null) {
			System.out.println("Unable to Establish the Connection!!!");
			return 0;
		}
				
		try {						
			pst = con.prepareStatement(qry);			
			pst.setInt(1, empId);			
			result = pst.executeUpdate();			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}



}