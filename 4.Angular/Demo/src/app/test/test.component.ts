import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {
          id : number;
          name: string;
          age: number;
          address : any;
          hobbies : any;
          constructor(){
            this.id = 85;
            this.name = "sree";
            this.age = 23;

            this.address = {
              streetNo : 101,
              city : "hyd",
              state : "telangana"
            };
            this.hobbies = ['Running', 'Walking', 'Swimming', 'Music', 'Cricket'];
          
          }
}
