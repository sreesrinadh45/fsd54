package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.Course;
import com.student.Student;
@Service
public class CourseDAO {
    @Autowired
    CourseRepository courseRepo;
//    public List<Course> getCourses(){
//    	List<Student> courseList = courseRepo.findAll();
//		return courseList;
//    }

	public Course getCourseById(int courseId) {
		return courseRepo.findById(courseId).orElse(null);
	}

	public Course getDepartmentByName(String courseName) {
		return courseRepo.findByName(courseName);
	}

	public Course getCourseByName(String courseName) {
		return courseRepo.findByName(courseName);
	}
	public Course addCourse(Course course) {
		return courseRepo.save(course);
	}
	public Course updateCourse(Course course) {
		return courseRepo.save(course);
	}
	public void deleteCourseById(int courseId) {
		courseRepo.deleteById(courseId);
	}
}
