package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.student.Student;

@Repository

public interface StudentRepository extends JpaRepository<Student, Integer> {

	@Query("from Student where studentName = :sName")
	Student findByName(@Param("sName") String studentName);

}
