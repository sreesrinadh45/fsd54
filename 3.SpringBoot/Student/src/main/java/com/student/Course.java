package com.student;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Course {
	@Id
	private int courseId;
	private String courseName;
	private String location;
	@JsonIgnoreProperties
	@OneToMany(mappedBy = "course")
	List<Student> studList = new ArrayList<Student>();

	public Course() {
		super();
	}

	public Course(int courseId, String courseName, String location) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.location = location;
	}

	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", courseName=" + courseName + ", location=" + location + ", studList="
				+ studList + "]";
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
