package com.ts.Student;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDAO;
import com.student.Student;


@RestController
public class StudentController {

	//Dependency Injection
	@Autowired
	StudentDAO studDAO;
	@GetMapping("getStudents")
	public List<Student> getStudents() {
		List<Student> studList = studDAO.getStudents();
		return studList;
	}
//	@GetMapping("getStudentById/{id}")
//	public Student getStudentById(@PathVariable("id") int studentId){
//		Student student = studDAO.getStudentById(studentId);
//		return student;
//	}
	@GetMapping("getStudentByName/{Name}")
	public Student getStudentByName(@PathVariable("name") String studentName){
		return studDAO.getStudentByName(studentName);
	}
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student){
		return studDAO.addStudent(student);
	}
	@PostMapping("updateStudent")
	public Student updateStudent(@RequestBody Student student){
		return studDAO.updateStudent(student);
	}
	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int studentId){
		studDAO.deleteStudentById(studentId);
		return "Student With StudentId: "  + studentId + ", Deleted Succcessfully";
	}
//	public Student getStudent(){
//		Student student = new Student();
//		student.setStudentId(101);
//		student.setStudentName("Sree");
//		student.setBatch(54);
//		return student;
//	}
//	@GetMapping("getAllStudents")
//	public List<Student> getAllStudents(){
//		
//		List<Student> studentList = new ArrayList<Student>();
//		Student student1 = new Student(101, "Sree", 54,  60000.00);
//		Student student2 = new Student(101, "Ram", 54, 60000.00);
//		Student student3 = new Student(101, "Gova", 56, 60000.00);
//		studentList.add(student1);
//		studentList.add(student2);
//		studentList.add(student3);
//		return studentList;
//		
//	}
	
}
