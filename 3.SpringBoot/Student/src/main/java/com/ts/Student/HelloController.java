package com.ts.Student;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	@RequestMapping("Hello")
	public String sayHello() {
         return "Hello Controller";
	}
}
