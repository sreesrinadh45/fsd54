package com.ts.Student;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {

	@RequestMapping("Hi") // hi - URL
	public String sayHi() {
		return "hi from HiController";
	}

	@RequestMapping("welcome") // welcome - URL
	public String welcome() {
		return "Welcome to SpringBoot";
	}

}
