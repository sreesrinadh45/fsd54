package com.ts.Student;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CourseDAO;
import com.student.Course;

@RestController
public class CourseController {
	@Autowired
	CourseDAO courseDao;

//	@GetMapping("getCourses")
//	public List<Course> getCourses() {
//		List<Course> courseList = courseDao.getCourses();
//		return courseList;
//	}

	@GetMapping("getCourseById/{id}")
	public Course getCourseById(@PathVariable("id") int courseId) {
		return courseDao.getCourseById(courseId);
	}

	@GetMapping("getCourseByName/{name}")
	public Course getCourseByName(@PathVariable("name") String courseName) {
		return courseDao.getCourseByName(courseName);
	}
}
