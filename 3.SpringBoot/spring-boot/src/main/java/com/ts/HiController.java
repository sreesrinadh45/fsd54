package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {
     
	  @RequestMapping("hi")   //hi url
	  public String  sayHi(){
		  return " hi in Hai Controller";
	  }
	  
	  @RequestMapping("Welcome")   //welcome url
	  public String  saywelcome(){
		  return "Welcome Controller";
	  }
}
