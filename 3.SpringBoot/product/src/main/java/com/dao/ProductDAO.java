package com.dao;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import com.model.product;

@Service
public class ProductDAO {
     
	
	//Dependency Injection
		@Autowired	
		ProductRepository productRepo;
		
		public List<product> getProducts() {
			List<product> prodList = productRepo.findAll();
			return prodList;
		}
		public product getProductById(int prodId) {
			product prod = new product(0, "Product Not Found!!!", 0.0);
			product product = productRepo.findById(prodId).orElse(prod);
			return product;
		}

		public product getProductByName(String prodName) {
			return productRepo.findByName(prodName);
		}
		public product addProduct(product product) {
			return productRepo.save(product);
		}
		public product updateProduct(product product) {
			return productRepo.save(product);
		}
		public void deleteProductById(int prodId) {
			productRepo.deleteById(prodId);
		}
}
