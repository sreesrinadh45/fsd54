package com.dao;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.product;

@Repository
public interface ProductRepository extends JpaRepository<product, Integer>{
   //@Autowired
  // Dependency Injection
   //ProductRepository prodDao;
	
	
		@Query("from product where productName = :pName")
		product findByName(@Param("pName") String productName);
		
	
}
