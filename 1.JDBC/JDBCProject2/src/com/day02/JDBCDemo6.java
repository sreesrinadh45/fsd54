package com.day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class JDBCDemo6 {
           public static void main(String[] args){
        	   Connection con = DbConnection.getConnection();
        	   PreparedStatement pst = null;
        	   if(con == null){
        		   System.out.println("DbConnection Failed!!!");
        		   return;
        	   }
        	   Scanner scan =  new Scanner(System.in);
        	   System.out.println("Enter EmpId : ");
        	   int empId = scan.nextInt();
        	   System.out.println();
        	   //Updating Password
        	   try{
        		   String deleteQry = "delete from employee where empId = " + empId;
        		  pst = con.prepareStatement(deleteQry);
        		  int result = pst.executeUpdate(deleteQry);
        		  if(result > 0){
        			  System.out.println("Record(s) Deleted..");
        		  }else{
        			  System.out.println("Failed to Delete The Employee Record..");
        		  }
        	   }catch(SQLException e1){
        		   e1.printStackTrace();
        	   }
        	   finally{
        		   if(con != null){
        			   try{
        				   pst.close();
        				   con.close();
        			   }catch (SQLException e){
        				   e.printStackTrace();
        			   }
        			   
        		   }
        	   }
           }
}
