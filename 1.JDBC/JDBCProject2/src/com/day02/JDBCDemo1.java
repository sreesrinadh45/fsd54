package com.day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

public class JDBCDemo1 {
	public static void main(String[] args) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			String selectQry = "Select * from employee";
			pst = con.prepareStatement(selectQry);
			rs = pst.executeQuery();			
			
			while(rs.next()) {
				System.out.print(rs.getInt(1)            + " ");
				System.out.print(rs.getString(2)         + " ");
				System.out.print(rs.getDouble("salary")  + " ");
				System.out.print(rs.getString(4)         + " ");
				System.out.print(rs.getString("loginId") + " ");
				System.out.println(rs.getString(6));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
}

