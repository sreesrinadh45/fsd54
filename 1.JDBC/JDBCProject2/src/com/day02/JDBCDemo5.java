package com.day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class JDBCDemo5 {
       public static void main(String[] args){
    	   Connection con = DbConnection.getConnection();
    	   PreparedStatement pst = null;
    	   
    	   if (con == null){
    		   System.out.println("DbConnection Failed!!!");
    		   return;
    	   }
    	   Scanner scan = new Scanner(System.in);
    	   System.out.println("Enter EmpId     :");
    	   int empId = scan.nextInt();
    	   System.out.println("Enter New Password: ");
    	   String password = scan.next();
    	   System.out.println();
    	   //Updating employee password
    	   
    	   try{
    		   String updateQry = "update employee set password = '" + password + "' where empId = " + empId;
    		   pst = con.prepareStatement(updateQry);
    		   int result = pst.executeUpdate(updateQry);
    		   if(result > 0){
    			   System.out.println(result + " Record(s) Updated...");
    		   }else{
    			   System.out.println("Failed to Update the Password of Employee Record...");
    		   }
    		   
    	   }catch(SQLException e1){
    		   e1.printStackTrace();
    	   }
    	   finally{
    		   if(con != null){
    			   try{
    				   pst.close();
    				   con.close();
    			   }catch(SQLException e){
    				   e.printStackTrace();
    			   }
    		   }
    	   }
       }
}
