package JDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class JDBCDemo5 {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;

		if (con == null) {
			System.out.println("DbConnection Failed!!!");
			return;
		}

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter EmpId       : ");
		int empId = scan.nextInt();
		System.out.print("Enter New Password: ");
		String password = scan.next();
		System.out.println();

		//Write our own code Updating the password for employee record in the database.
		try {
			String updateQry = "update employee set password = '" + password + "' where empId = " + empId;
			st = con.createStatement();
			int result = st.executeUpdate(updateQry);

			if (result > 0) {				
				System.out.println(result + " Record(s) Updated...");
			} else {
				System.out.println("Failed to Update the Password of Employee Record...");
			}

		} catch (SQLException e1) {
			e1.printStackTrace();
		}


		finally {
			if (con != null) {
				try {
					st.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
