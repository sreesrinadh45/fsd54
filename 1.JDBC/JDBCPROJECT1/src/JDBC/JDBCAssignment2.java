package JDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCAssignment2 {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;
		ResultSet rs = null;

		if (con == null) {
			System.out.println("DbConnection Failed!!!");
			return;
		}

		try {
			String selectQry = "select salary, count(salary),salary from employee group by salary having salary >3000";
			st = con.createStatement();
			rs = st.executeQuery(selectQry);

			int count = 0;
			while (rs.next()) {
				count++;
			}

			
			if (count > 0) {
				System.out.println("The Employee count of Salary between 3000 and 6000: " + count);
			} else {
				System.out.println("No Salary(s)");
			}

		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			if (con != null) {
				try {
					if (rs != null) {
						rs.close();
					}
					if (st != null) {
						st.close();
					}
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}