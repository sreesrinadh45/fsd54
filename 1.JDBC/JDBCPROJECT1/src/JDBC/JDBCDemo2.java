package JDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class JDBCDemo2 {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;
		ResultSet rs = null;

		if (con == null) {
			System.out.println("DbConnection Failed!!!");
			return;
		}

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter Employee Id: ");
		int empId = scan.nextInt();
		System.out.println();

		//Write our own code for fetching the employee record based on employeeId.
		try {
			st = con.createStatement();
			rs = st.executeQuery("select * from employee where empId = " + empId);

			if (rs.next()) {				
				System.out.println("Employee Details");
				System.out.println("----------------");
				System.out.println("EmpId    : " + rs.getInt(1));
				System.out.println("EmpName  : " + rs.getString(2));
				System.out.println("Salary   : " + rs.getDouble(3));
				System.out.println("Gender   : " + rs.getString(4));
				System.out.println("LoginId  : " + rs.getString(5));

			} else {
				System.out.println("Employee Record Not Found!!!");
			}

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					rs.close();
					st.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
