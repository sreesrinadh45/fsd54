package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcDemo1 {
	public static void main(String[] args) {

		try {
			//1.Loading the Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			//Establishing the Connection
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/fsd54", "root", "root");
			
			//Check the Connection object
			if (con != null) {
				System.out.println("Connection Established...");
				
				con.close();
				
			} else {
				System.out.println("Failed to Establish the Connection...");
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}

