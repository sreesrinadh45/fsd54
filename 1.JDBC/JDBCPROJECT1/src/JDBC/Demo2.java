package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Demo2 {
	public static void main(String[] args) {

		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/fsd54";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");			
			con = DriverManager.getConnection(url, "root", "root");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} 
		
		if (con != null) {
			System.out.println("Connection Established...");
		
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} else {
			System.out.println("Connection Established Failed...");
		}
	}
}
