package JDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class JDBCDemo3 {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;
		ResultSet rs = null;

		if (con == null) {
			System.out.println("DbConnection Failed!!!");
			return;
		}

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter LoginId : ");
		String loginId = scan.next();
		System.out.print("Enter Password: ");
		String password = scan.next();
		System.out.println();

		//Write our own code for fetching the employee record based on employeeId.
		try {
			String selectQry = "select * from employee where loginId = '" + loginId + "' and password = '" + password + "'";
			st = con.createStatement();
			rs = st.executeQuery(selectQry);

			if (rs.next()) {				
				System.out.println("Employee Details");
				System.out.println("----------------");
				System.out.println("EmpId    : " + rs.getInt(1));
				System.out.println("EmpName  : " + rs.getString(2));
				System.out.println("Salary   : " + rs.getDouble(3));
				System.out.println("Gender   : " + rs.getString(4));
				System.out.println("LoginId  : " + rs.getString(5));
				System.out.println("Password : " + rs.getString(6));
				

			} else {
				System.out.println("Employee Record Not Found!!!");
			}

		} catch (SQLException e1) {
			e1.printStackTrace();
		}


		finally {
			if (con != null) {
				try {
					rs.close();
					st.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

