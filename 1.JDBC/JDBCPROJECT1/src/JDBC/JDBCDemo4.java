package JDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class JDBCDemo4 {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;

		if (con == null) {
			System.out.println("DbConnection Failed!!!");
			return;
		}

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter EmpId   : ");
		int empId = scan.nextInt();
		System.out.print("Enter EmpName : ");
		String empName = scan.next();
		System.out.print("Enter Salary  : ");
		double salary = scan.nextDouble();
		System.out.print("Enter Gender  : ");
		String gender = scan.next();
		System.out.print("Enter LoginId : ");
		String loginId = scan.next();
		System.out.print("Enter Password: ");
		String password = scan.next();
		System.out.println();

		//Write our own code inserting the employee record in the database.
		try {
			String insertQry = "insert into employee values (" + empId + ", '" + empName + "', " + salary + ", '" + gender + "', '" + loginId + "', '" + password + "')";
			st = con.createStatement();
			int result = st.executeUpdate(insertQry);

			if (result > 0) {				
				System.out.println(result + " Record(s) inserted...");
			} else {
				System.out.println("Failed to Insert the Employee Record...");
			}

		} catch (SQLException e1) {
			e1.printStackTrace();
		}


		finally {
			if (con != null) {
				try {
					st.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
