package JDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class JDBCDemo {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;
		ResultSet rs = null;
				
		if (con == null) {
			System.out.println("DbConnection Failed!!!");
			return;
		}		
		System.out.println("Connection Established Successfully...");
		
		
		//Write our own code for fetching the data.
		try {
			st = con.createStatement();
			rs = st.executeQuery("select * from employee");
			
			if (rs != null) {
				
				while (rs.next()) {
				
					System.out.print(rs.getInt(1)            + " ");
					System.out.print(rs.getString(2)         + " ");
					System.out.print(rs.getDouble("salary")  + " ");
					System.out.print(rs.getString(4)         + " ");
					System.out.print(rs.getString("loginId") + " ");
					System.out.println(rs.getString(6));
				}
				
			} else {
				System.out.println("No Data Found!!!");
			}
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		

		finally {
			if (con != null) {
				try {
					rs.close();
					st.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

